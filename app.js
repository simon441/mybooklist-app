// Book Class : Represents a Book
class Book {
  constructor(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}

// UI Class: Handles Storage
class UI {
  // timeout of the alert
  timeout = 3000;

  /**
   * Displayb the list of Books
   */
  static displayBooks() {
    const books = Store.getBooks();

    books.forEach(book => UI.addBookToList(book));
  }

  /**
   * Add a book to the list of books
   * @param {Book} book
   */
  static addBookToList(book) {
    const list = document.querySelector('#book-list');

    const row = document.createElement('tr');

    row.innerHTML = `
        <td>${book.title}</td>
        <td>${book.author}</td>
        <td>${book.isbn}</td>
        <td><a href="#" class="btn btn-danger btn-sm delete">X</td>
    `;
    list.appendChild(row);
  }

  /**
   * Delete ea Book
   * @param {HTMLElement} el
   */
  static deleteBook(el) {
    if (el.classList.contains('delete')) {
      el.parentElement.parentElement.remove();
    }
  }

  /**
   * Show a bootstrap Alert
   *
   * @param {String} message Message to display
   * @param {String} className End of classname to use: success, default, danger, info, ...
   */
  static showAlert(message, className) {
    const div = document.createElement('div');
    div.className = `alert alert-${className}`;
    div.appendChild(document.createTextNode(message));
    const container = document.querySelector('.container');
    const form = document.querySelector('#book-form');
    container.insertBefore(div, form);

    // Vanishing 3 seconds
    setTimeout(() => document.querySelector('.alert').remove(), 3000);
  }

  /**
   * Clear the form fields
   */
  static clearFields() {
    document.querySelector('#title').value = '';
    document.querySelector('#author').value = '';
    document.querySelector('#isbn').value = '';
  }
}

/**
 * @class Store class
 * Store class: Handles Storage
 */
class Store {
  /**
   * @returns Book[]
   */
  static getBooks() {
    let books;
    if (localStorage.getItem('books') === null) {
      books = [];
    } else {
      books = JSON.parse(localStorage.getItem('books'));
    }

    return books;
  }

  /**
   *
   * @param {Book} book
   */
  static addBook(book) {
    const books = Store.getBooks();

    books.push(book);

    localStorage.setItem('books', JSON.stringify(books));
  }

  /**
   *
   * @param {String} isbn
   */
  static removeBook(isbn) {
    const books = Store.getBooks();
    books.forEach((book, index) => {
      if (book.isbn === isbn) {
        books.splice(index, 1);
      }
    });

    localStorage.setItem('books', JSON.stringify(books));
  }
}

// Event: Display Books
document.addEventListener('DOMContentLoaded', () => UI.displayBooks());

// Event: Add a Book
document.querySelector('#book-form').addEventListener('submit', e => {
  // Prevent actual submit
  e.preventDefault();

  // Get form values
  const title = document.querySelector('#title').value;
  const author = document.querySelector('#author').value;
  const isbn = document.querySelector('#isbn').value;

  // Validate
  if (title == '' || author == '' || isbn == '') {
    UI.showAlert('Please fill in the all the fields', 'danger');
  } else {
    // Instantiate book
    const book = new Book(title, author, isbn);

    //   console.log(book);

    // Add Book to IU
    UI.addBookToList(book);

    // Add Book to Store
    Store.addBook(book);

    // Show success message
    UI.showAlert('Book Added', 'success');

    UI.clearFields();
  }
});

// Event: Remove a Book
document.querySelector('#book-list').addEventListener('click', e => {
  e.preventDefault();

  UI.deleteBook(e.target);

  // Remove Book from Store
  Store.removeBook(e.target.parentElement.previousElementSibling.textContent);

  // Show success message
  UI.showAlert('Book Removed', 'success');
});
